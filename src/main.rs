#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate regex;
extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate failure;

use failure::Error;
use regex::Regex;
use rocket::fairing::AdHoc;
use rocket::http::{Cookie, Cookies, Status};
use rocket::request::Form;
use rocket::response::{Failure, NamedFile, Redirect};
use rocket::State;
use rocket_contrib::Template;
use std::collections::HashMap;
use std::io::{prelude::*, BufReader};
use std::process::{Command, Stdio};
use std::sync::{Arc, Mutex};
use std::thread;

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
enum VideoStatus {
    Starting,
    Downloading(DownloadProgress),
    Finished,
    Error,
}

#[derive(Debug, Serialize)]
struct DownloadProgress {
    progress: String,
    size: String,
    speed: String,
    eta: String,
}

#[derive(Clone)]
struct AppConfig {
    output_dir: String,
    php_exec: String,
    occ_path: String,
    nextcloud_path: String,
    token: String,
}

#[derive(Debug, Fail)]
enum AppError {
    #[fail(display = "Error while spawning process.")]
    SpawnError,
    #[fail(display = "Error while locking state.")]
    LockError,
}

#[derive(FromForm)]
struct VideoForm {
    pub video: String,
}

impl DownloadProgress {
    pub fn new(progress: String, size: String, speed: String, eta: String) -> DownloadProgress {
        DownloadProgress {
            progress,
            size,
            speed,
            eta,
        }
    }
}

type DownloadState = Arc<Mutex<HashMap<String, VideoStatus>>>;

fn download(id: String, ds: DownloadState, conf: AppConfig) -> Result<(), Error> {
    let mut command = Command::new("youtube-dl")
        .arg("-f best")
        .arg("--newline")
        .arg(id.clone())
        .current_dir(conf.output_dir)
        .stdout(Stdio::piped())
        .spawn()?;
    {
        let stdout = command.stdout.as_mut().ok_or(AppError::SpawnError)?;
        let reader = BufReader::new(stdout);
        let re = Regex::new(r"^\D*(\d[^ ]*)\D*(\d[^ ]*)\D*(\d[^ ]*)\D*(\d[^ ]*)")?;
        for line in reader.lines() {
            if let Ok(l) = line {
                if let Some(c) = re.captures(&l) {
                    let mut state = ds.lock().map_err(|_| AppError::LockError)?;
                    let progress = c[1].to_string();
                    let size = c[2].to_string();
                    let speed = c[3].to_string();
                    let eta = c[4].to_string();
                    state.insert(
                        id.clone(),
                        VideoStatus::Downloading(DownloadProgress::new(progress, size, speed, eta)),
                    );
                }
            }
        }
    }
    let mut state = ds.lock().map_err(|_| AppError::LockError)?;
    if command.wait()?.success() {
        let mut occ = Command::new(conf.php_exec)
            .arg(conf.occ_path)
            .arg("files:scan")
            .arg("--path")
            .arg(conf.nextcloud_path)
            .spawn()?;
        if occ.wait()?.success() {
            state.insert(id, VideoStatus::Finished);
        } else {
            state.insert(id, VideoStatus::Error);
        }
    } else {
        state.insert(id, VideoStatus::Error);
    }
    Ok(())
}

fn check_token(mut cookies: Cookies, conf: &AppConfig) -> Result<(), Failure> {
    let token = cookies.get_private("token").ok_or(Status::Unauthorized)?;
    if token.value() != conf.token {
        return Err(Failure(Status::Unauthorized));
    }
    Ok(())
}

#[get("/")]
fn index(cookies: Cookies, conf: State<AppConfig>) -> Result<NamedFile, Failure> {
    check_token(cookies, &conf)?;
    Ok(NamedFile::open("static/index.html").unwrap())
}

#[post("/", data = "<video>")]
fn index_post(
    cookies: Cookies,
    video: Form<VideoForm>,
    conf: State<AppConfig>,
) -> Result<Redirect, Failure> {
    check_token(cookies, &conf)?;
    let s = video.get().video.clone();
    let v: Vec<&str> = s.split('=').collect();
    let id = if v.len() > 1 {
        v.last().unwrap()
    } else {
        v.get(0).unwrap()
    };
    Ok(Redirect::to(&format!("/video/{}", id)))
}

#[get("/video/<id>")]
fn video(
    cookies: Cookies,
    id: String,
    ds: State<DownloadState>,
    conf: State<AppConfig>,
) -> Result<Template, Failure> {
    check_token(cookies, &conf)?;
    let (res, remove) = {
        let mut state = ds.lock().unwrap();
        let entry = {
            let id = id.clone();
            state.entry(id.clone()).or_insert_with(|| {
                let ds = ds.clone();
                let conf = (*conf).clone();
                thread::spawn(move || {
                    if download(id.clone(), ds.clone(), conf).is_err() {
                        let mut state = ds.lock().unwrap();
                        state.insert(id, VideoStatus::Error);
                    }
                });
                VideoStatus::Starting
            })
        };
        let remove = match entry {
            VideoStatus::Error => true,
            VideoStatus::Finished => true,
            _ => false,
        };
        (Template::render("video", &entry), remove)
    };
    if remove {
        let mut state = ds.lock().unwrap();
        state.remove(&id);
    }
    Ok(res)
}

#[get("/token/<token>")]
fn token(mut cookies: Cookies, token: String) -> Result<Redirect, Failure> {
    let re = Regex::new(r"[0-9a-zA-Z+/]?").map_err(|_| Failure(Status::InternalServerError))?;
    if !re.is_match(&token) {
        return Err(Failure(Status::BadRequest));
    }
    cookies.add_private(
        Cookie::parse(format!("token={}; SameSite=Lax; Max-Age=3600", token))
            .map_err(|_| Failure(Status::BadRequest))?,
    );
    Ok(Redirect::to("/"))
}

fn main() {
    let downloadstate: DownloadState = Arc::new(Mutex::new(HashMap::new()));
    rocket::ignite()
        .mount("/", routes![index, index_post, token, video])
        .manage(downloadstate)
        .attach(Template::fairing())
        .attach(AdHoc::on_attach(|rocket| {
            let conf = AppConfig {
                output_dir: rocket.config().get_str("output_dir").unwrap().to_string(),
                php_exec: rocket.config().get_str("php_exec").unwrap().to_string(),
                occ_path: rocket.config().get_str("occ_path").unwrap().to_string(),
                nextcloud_path: rocket
                    .config()
                    .get_str("nextcloud_path")
                    .unwrap()
                    .to_string(),
                token: rocket.config().get_str("token").unwrap().to_string(),
            };
            Ok(rocket.manage(conf))
        }))
        .launch();
}
